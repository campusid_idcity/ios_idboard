//
//  Utility.swift
//  idboard
//
//  Created by Marius Marciniak on 02/02/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation
import UIKit

class Utility {
    
    static func printHello() {
        print("Hello world !")
    }
    
    /*
     * Recupere une variable persistente en mémoire
    */
    static func getUserDefaults(key: String) -> Any? {
        if let value = UserDefaults.standard.object(forKey: key) {
            return value
        }
        else {
            return nil
        }
    }
    
    /*
     * Recupere une variable persistente en mémoire sous format JSON
     */
    static func getUserDefaultsJson<T:Decodable>(key: String, decodingType: T.Type) -> T? {
        if let data = UserDefaults.standard.data(forKey: key) {
            do {
                let dataDecoded = try JSONDecoder().decode(decodingType, from: data)
                return dataDecoded
            } catch let jsonErr {
                print("Erreur decoding json: ", jsonErr)
                return nil
            }
        }
        else {
            return nil
        }
    }

    /*
     * Ajoute une variable persistente en mémoire sous format JSON
     */
    static func setUserDefaultsJson<T:Encodable>(object: T?, key: String) {
        if let object = object {
            do {
                let objectJson = try JSONEncoder().encode(object)
                UserDefaults.standard.setValue(objectJson, forKey: key)
            } catch let jsonErr {
                print("Erreur enconding json: ", jsonErr)
            }
        }

    }
    
    /*
    * Converti une couleur hexadécimal en UIColor
    */
    static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
}
