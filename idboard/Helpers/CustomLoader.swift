//
//  CustomLoader.swift
//  idboard
//
//  Created by Marius Marciniak on 20/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation
import UIKit

class CustomLoader: UIView {
    
    static let instance = CustomLoader()
    
    lazy var transparentView: UIView = {
        //var transparentView = UIView(frame: UIScreen.main.bounds)
        let transparentView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - 49.0))
        transparentView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        transparentView.isUserInteractionEnabled = false
        return transparentView
    }()
    
    lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.center = transparentView.center
        activityIndicator.startAnimating()
        return activityIndicator
    }()
    
    // Animation indicateur setup
    func setupActivityIndicator(controller: UIViewController) {
        self.transparentView.addSubview(activityIndicator)
        controller.navigationController?.view.addSubview(transparentView)
        controller.navigationController?.view.isUserInteractionEnabled = false
    }
    
    func hideActivityIndicator(controller: UIViewController) {
        controller.navigationController?.view.sendSubviewToBack(self.transparentView)
        self.transparentView.removeFromSuperview()
        controller.navigationController?.view.isUserInteractionEnabled = true
        UIApplication.shared.endIgnoringInteractionEvents()
    }
}
