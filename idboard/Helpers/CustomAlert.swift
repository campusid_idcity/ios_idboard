//
//  CustomAlert.swift
//  idboard
//
//  Created by Marius Marciniak on 18/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation
import UIKit

class CustomAlert {
    
    func showAlertReturn(title:String, message:String, controller: UIViewController, navigationController: UINavigationController?) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Retour", style: .default) { (action) in
            if let navigation = navigationController {
                print("ciao")
                navigation.popViewController(animated: true)
            }
        }
        
        alertController.addAction(alertAction)
        controller.present(alertController, animated: true, completion: nil)
    }
}
