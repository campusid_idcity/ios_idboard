//
//  UserDefaultKey.swift
//  idboard
//
//  Created by Martin on 03/04/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation
struct UserDefaultsKey {
    static let USER_IS_LOGGED : String = "userIsLogged"
    static let USER_ID : String = "userId"
    
    static let BUSINESS_ENTITIES_CONNECTED: String = "businessEntitiesConnected"
}
