//
//  AppDelegate.swift
//  idboard
//
//  Created by Marius Marciniak on 02/02/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {


    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        /* Avec l'arrivé de WindowScene, c'est dans SceneDelegate.swift => func scene() que l'on peut créer le point d'entrée de l'application par le code
         =======
            In Xcode 11, the window now belongs to the scene delegate. That is where you need to create the window and set its root view controller.
         =======
         */
        let color = UIColor(red: 193/255.0, green: 10.0/255.0, blue: 10.0/255.0, alpha: 1.0)
        
        if #available(iOS 11.0, *) {
            //To change iOS 11 navigationBar largeTitle color
            //UINavigationBar.appearance().prefersLargeTitles = true
            //UINavigationBar.appearance().largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: color]
        }
        
        // Couleur des icones de la tab bar
        UITabBar.appearance().tintColor = color
        //UITabBar.appearance().barTintColor = color
        // Couleur des boutons dans la nav bar
        UINavigationBar.appearance().tintColor = color
        //UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: color]
        //UINavigationBar.appearance().backgroundColor = color
        
        //UINavigationBar.appearance().barTintColor = color
    
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

