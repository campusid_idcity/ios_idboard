//
//  BusinessEntitiesDto.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 18/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct BusinessEntitiesDto : Codable {
    let idBusinessEntities: Int?
    let idSalutation : Int?
    let idTypeBusinessEntity : Int?
    let idSystemLevel: Int?
    let idCurrentClass: Int?
    let currentClassName: String?
    let idboard : String?
    let name : String?
    let firstName: String?
    let surName: String?
    let photoPath : String?
    let dateOfBirth : String?
    let placeOfBirth: String?
    let nationality: String?
    let countryOfBirth : String?
    let comments : String?
    let cardTokenId: String?
}
