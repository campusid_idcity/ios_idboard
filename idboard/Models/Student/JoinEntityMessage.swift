//
//  JoinEntityMessage.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 04/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct JoinEntityMessage : Codable {
    let idJoin : Int?
    let idBusinessEntity : Int?
    let idMessage : Int?
}
