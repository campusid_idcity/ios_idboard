//
//  TypesCourses.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 17/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct  TypesCourses : Codable {
    let idTypesCourse : Int?
    let typeDefaultValue : String?
    let backgroungColor : String?
    let barColor : String?
    let fontColor : String?
    let courses : [Courses]?
    let typesCoursesGlobalization : [TypesCoursesGlobalization]?
}
