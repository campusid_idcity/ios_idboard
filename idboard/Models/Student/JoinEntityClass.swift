//
//  JoinEntityClass.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 17/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct JoinEntityClass : Codable {
    let idJoin : Int?
    let idBusinessEntity : Int?
    let idClass : Int?
}
