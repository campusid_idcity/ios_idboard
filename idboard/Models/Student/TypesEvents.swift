//
//  TypesEvents.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 18/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct TypesEvents : Codable {
    let idTypeEvent: Int?
    let typeDefaultValue : String?
    let events : [Events]?
    let typesEventsGlobalization : [TypesEventsGlobalization]?
}
