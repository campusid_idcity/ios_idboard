//
//  Sites.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 18/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct Sites : Codable {
    let idSite : Int?
    let name : String?
    let address : String?
    let classes : [Classes]?
    let roles : [Roles]?
}

