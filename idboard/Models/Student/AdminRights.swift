//
//  AdminRights.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 18/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct AdminRights : Codable {
    let idAdminRight: Int?
    let idTypeRole : Int?
    let idSystemLevel : Int?
    let basicRights: Bool?
    let classesRights: Bool?
    let planningRights : Bool?
    let marksRights : Bool?
    let internshipOffersRights: Bool?
    let internshipConventionsRights: Bool?
    let impersonationRights : Bool?
    let logsRights : Bool?
    let importationRights: Bool?
    let preregistrationRights: Bool?
    let alterationDataRights : Bool?
    let academicLifeRights : Bool?
    let securityRights: Bool?
    let mattersCoursesPlansRights : Bool?
}
