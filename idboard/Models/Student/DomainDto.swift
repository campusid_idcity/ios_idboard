//
//  Domaine.swift
//  idboard
//
//  Created by Marius Marciniak on 18/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct DomainDto : Codable {
    let idDomain:Int?
    let descriptionDefaultValueDomain:String?
    let ectscredits:Double?
    let isValidate:Int?
    let mediumOfIdIdentifiant:Double?
    let mediumOfClass:Double?
    let matters:[MattersDto]?
}
