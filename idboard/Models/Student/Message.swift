//
//  Message.swift
//  idboard
//
//  Created by CampusID001 on 27/03/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation
//Variable utiliser pour l'affichage des messages dans cette onglet
struct Message: Codable{
    let idMessage: Int?
    let title: String?
    let bbcode: String?
    let dateStart: String?
    let dateEnd: String?
    let priority: Bool?
    let administrativeRequest: Bool?
    let modificationRequest: Bool?
    
    let clientsDataModificationRequests : [ClientsDataModificationRequests]?
    let joinEntityMessage : [JoinEntityMessage]?
    
}
