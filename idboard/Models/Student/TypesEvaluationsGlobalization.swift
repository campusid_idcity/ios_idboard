//
//  TypesEvaluationsGlobalization.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 17/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct TypesEvaluationsGlobalization : Codable {
    let idTypeEvaluationGlobalization : Int?
    let idLanguage : Int?
    let elementValue : String?
    let idTypeEvaluation : Int?
}
