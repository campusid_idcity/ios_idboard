//
//  MattersGlobalization.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 05/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct MattersGlobalization : Codable {
    let idMattersGlobalization : Int?
    let idLanguage : Int?
    let elementValue : String?
    let idMatter : Int?
}
