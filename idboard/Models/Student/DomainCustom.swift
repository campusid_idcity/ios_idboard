//
//  DomainCustom.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 18/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct DomainCustom : Codable {
    let idDomain : Int?
    let descriptionDefaultValueDomain : String?
    let ectscredits : Double?
    let isValidate : Bool?
    let mediumOfIdIdentifiant : Double?
    let mediumOfClass : Double?
    let matters : [MattersCustom]?
}
