//
//  SalutationsDto.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 18/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct SalutationsDto : Codable {
    let idSalutation : Int?
    let title : String?
}
