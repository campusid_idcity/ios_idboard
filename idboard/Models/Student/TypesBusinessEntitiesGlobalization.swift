//
//  TypesBusinessEntitiesGlobalization.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 04/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct TypesBusinessEntitiesGlobalization : Codable {
    let idTypesBusinessEntitiesGlobalization : Int?
    let idLanguage : Int?
    let elementValue : String?
    let idTypeBusinessEntity : Int?
}
