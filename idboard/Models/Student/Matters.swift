//
//  Matters.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 05/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct Matters : Codable {
    let idMatter : Int?
    let idClass : Int?
    let reference : String?
    let descriptionDefaultValue : String?
    let marksNumber : Int?
    let ectscredits : Double?
    let identifier : String?
    let idClassNavigation : [Classes]?
    let chapters : [Chapters]?
    let courses : [Courses]?
    let joinMatterDomain : [JoinMatterDomain]?
    let marks : [Marks]?
    let mattersGlobalization : [MattersGlobalization]?
}
