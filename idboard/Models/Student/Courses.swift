//
//  Courses.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 17/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct Courses : Codable {
    let idCourse : Int?
    let idClass : Int?
    let dateStart : String?
    let dateEnd : String?
    let teacherName : String?
    let teacherFirstname : String?
    let teacherSurname : String?
    let descriptionDefaultValue : String?
    let typeDefaultValue : String?
    let backGroundColor : String?
    let fontColor : String?
    
    public func checkDate(dateDebut:String) -> Bool{
        
        var result:Bool = false
        
        if dateDebut.prefix(10) == dateStart?.prefix(10) {
           result = true
        }
        
        return result
        
    }
}
