//
//  Aspects.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 04/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct Aspects : Codable {
    let idAspect : Int?
    let coefficient : String?
    let descriptionDefaultValue : String?
}
