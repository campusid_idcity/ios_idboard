//
//  TypesInternships.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 17/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct TypesInternships : Codable {
    let idTypeInternship : Int?
    let typeDefaultValue : String?
    let internshipConventionRequests : [InternshipConventionRequests]?
    let internshipOffers : [InternshipOffers]?
    let typesInternshipsGlobalization : [TypesInternshipsGlobalization]?
}
