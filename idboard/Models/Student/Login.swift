//
//  Login.swift
//  idboard
//
//  Created by CampusID002 on 04/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation
struct Login: Codable {
    let userId:Int?
    let id:Int?
    let title :String?
    let body:String?
}
// Struc pour retour api élève
struct BusinessEntity: Codable {
    let idBusinessEntity: Int?
    let idSalutation: Int?
    let idTypeBusinessEntity: Int?
    let idSystemLevel: Int?
    let idCurrentClass: Int?
    let idboard: String?
    let name: String?
    let firstName: String?
    let surName: String?
    let photoPath: String?
    let dateOfBirth : String?
    let placeOfBirth : String?
    let nationality : String?
    let countryOfBirth: String?
    let comments: String?
    let cardTokenId: Int?
}

struct Reset: Codable {
    let isExist:Bool?
}
