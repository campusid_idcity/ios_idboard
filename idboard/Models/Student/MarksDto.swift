//
//  MarksDto.swift
//  idboard
//
//  Created by Marius Marciniak on 20/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct MarksDto: Codable {
    let idMark:Int?
    let idMatter:Int?
    let idClass:Int?
    let idTypeEvaluation:Int?
    let coefficient:Double?
    let value:Double?
    let mediumOfClasses:Double?
    let comments:String?
    let dateCreation:String?
    let isJustifiedAbsence:Bool?
    let idTypeEvaluationNavigation: TypesEvaluations?
    
}
