//
//  CompetencesStatusGlobalization.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 17/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct CompetencesStatusGlobalization : Codable {
    let idCompetenceStatusGlobalization : Int?
    let idLanguage : Int?
    let elementValue : String?
    let idCompetenceStatus : Int?
}
