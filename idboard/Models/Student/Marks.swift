//
//  Marks.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 17/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct Marks : Codable {
    let idMark : Int?
    let idMatter : Int?
    let idClass : Int?
    let idTypeEvaluation : Int?
    let coefficient : Double
    let value : Double
    let mediumOfClasses : String?
    let comments : String?
    let dateCreation : String?
    let isJustifiedAbsence :Bool
}
