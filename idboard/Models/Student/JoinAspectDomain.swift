//
//  JoinAspectDomain.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 04/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct JoinAspectDomain : Codable {
    let idJoinAspectDomain : Int?
    let idAspect : Int?
    let idDomain : Int?
}
