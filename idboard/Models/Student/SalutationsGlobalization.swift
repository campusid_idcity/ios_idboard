//
//  SalutationsGlobalization.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 04/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct SalutationsGlobalization : Codable {
    let idSalutationsGlobalization : Int?
    let idLanguage : Int?
    let SalutationsGlobalization : String?
    let idSalutation : Int?
}
