//
//  StudentInformation.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 29/05/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//


import Foundation

struct  ContactDetails : Codable {
    let idContactDetails : Int?
    let idBusinessEntity : Int?
    let address1 : String?
    let address2 : String?
    let postalCode : String?
    let city : String?
    let country : String?
    let dateStart : String?
    let dateEnd : String?
}


