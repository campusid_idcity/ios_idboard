//
//  Classes.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 17/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct Classes : Codable {
    let idClass : Int?
    let idSite : Int?
    let name : String?
    let dateStart : String?
    let dateEnd : String?
    let preregDateStart : String?
    let preregDateEnd : String?
    let useBtstranscript : Bool
    let idSiteNavigation : [Sites]?
    let courses : [Courses]?
    let joinEntityClass : [JoinEntityClass]?
    let joinOfferClass : [JoinOfferClass]?
    let marks : [Marks]?
    let matters : [Matters]?
}
