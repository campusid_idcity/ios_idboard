//
//  Competences.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 17/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct Competences : Codable {
    let idCompetence : Int?
    let idChapter : Int?
    let name : String?
}
