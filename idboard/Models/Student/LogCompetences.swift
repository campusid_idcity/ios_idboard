//
//  LogCompetences.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 17/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct LogsCompetences : Codable {
    let idLogCompetence : Int?
    let idBusinessEntity : Int?
    let idCompetence: Int?
    let idCompetenceStatus : Int?
    let date : String?
}
