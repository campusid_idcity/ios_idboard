//
//  ConnexionsPreEnrolment.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 04/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct  LogsPreEnrolment : Codable {
    let idLogPreEnrolment : Int?
    let idBusinessEntity : Int?
    let date : String?
    let ipaddress : String?
}
