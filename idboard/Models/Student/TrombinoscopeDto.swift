//
//  TrombinoscopeDto.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 18/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct TrombinoscopeDto : Codable {
    let city : String?
    let name : String?
    let scholarYear : String?
    let totalStudentsInClass : Int?
    let students : [BusinessEntities]?
}
