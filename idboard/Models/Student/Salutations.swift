//
//  Salutations.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 04/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct Salutations : Codable {
    let idSalutation : Int?
    let salutationDefaultValue : String?
    let businessEntities : [BusinessEntities]?
    let internshipContacts : [InternshipContacts]?
    let salutationsGlobalization : [SalutationsGlobalization]?
}
