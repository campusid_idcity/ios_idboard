//
//  TypesRoles.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 18/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct TypesRoles : Codable {
    let idTypeRole: Int?
    let typeDefaultValue : String?
    let isStudent : Bool?
    let isAdvanceEnrolled: Bool?
    let isTeacher : Bool?
    let isCandidate : Bool?
    let isRepresentative : Bool?
    let isTutor: Bool?
    let isRelative : Bool?
    let className : String?
    let adminRights: [AdminRights]?
    let roles : [Roles]?
    let typesRolesGlobalization : [TypesRolesGlobalization]?
}
