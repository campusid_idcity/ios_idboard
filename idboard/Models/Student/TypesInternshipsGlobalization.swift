//
//  Typ/Users/lvs/Desktop/ios_idboard/idboard/Models/StudentesInternshipsGlobalization.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 17/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct TypesInternshipsGlobalization : Codable {
    let idTypesInternshipsGlobalization : Int?
    let idLanguage : Int?
    let elementValue : String?
    let idTypeInternship : Int?
}
