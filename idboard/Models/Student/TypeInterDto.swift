//
//  TypeInterDto.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 18/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct TypeInterDto : Codable {
    let idTypeInternship : Int?
    let typeDefaultValue : Int?
}
