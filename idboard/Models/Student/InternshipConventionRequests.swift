//
//  InternshipConventionRequests.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 04/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct  InternshipConventionRequests : Codable {
    let idRequest : Int?
    let idRepresentative : Int?
    let idTutor : Int?
    let idStudent : Int?
    let idTypeInternship : Int?
    let idLanguage : Int?
    let status : Int?
    let dateCreation : String?
    let dateEdition : String?
    let dateReturn : String?
    let dateStart : String?
    let dateEnd : String?
    let fieldsToModify : String?
    let dailyTasks : String?
    let technologicalEnvironment : String?
    let teachingGoals : String?
    let company : String?
    let phone : String?
    let fax : String?
    let mail : String?
    let webSite : String?
    let wage : Double?
    let typeWage : Bool?
    let currency : String?
    let wagePaymentPeriod : Int?
    let comments : String?
    let address1 : String?
    let address2 : String?
    let postalCode : String?
    let city : String?
    let country : String?
}
