//
//  Domains.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 04/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct Domains : Codable {
    let idDomain : Int?
    let descriptionDefaultValue : String?
    let domainsGlobalization : [Domains]?
    let joinAspectDomain : [JoinAspectDomain]?
    let joinMatterDomain : [JoinMatterDomain]?
}
