//
//  BirthdayDateDto.swift
//  idboard
//
//  Created by Marius Marciniak on 25/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct BirthdayDateDto: Codable {
    let dateOfToday: String?
    let studentName: String?
    let currentClass: String?
    let age: Int?
    let birthdaySentence: String?
}
