//
//  Informations.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 18/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct Informations : Codable {
    let idInformation: Int?
    let idBusinessEntity : Int?
    let idTypeInformation : Int?
    let idSystemLevel: Int?
    let value : String?
    let dateStart : String?
    let dateEnd: String?
}
