//
//  Languages.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 04/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct  Languages : Codable {
    let idLanguage : Int?
    let uiculture : String?
    let aspectsGlobalization : [AspectsGlobalization]?
    let competencesStatusGlobalization : [CompetencesStatusGlobalization]?
    let domainsGlobalization : [DomainsGlobalization]?
    let internshipConventionRequests : [InternshipConventionRequests]?
    let mattersGlobalization : [MattersGlobalization]?
    let salutationsGlobalization : [SalutationsGlobalization]?
    let systemLevelsGlobalization : [SystemLevelsGlobalization]?
    let typesBusinessEntitiesGlobalization : [TypesBusinessEntitiesGlobalization]?
    let typesCoursesGlobalization : [TypesCoursesGlobalization]?
    let typesEvaluationsGlobalization : [TypesEvaluationsGlobalization]?
    let typesEventsGlobalization : [TypesEventsGlobalization]?
    let typesInformationsGlobalization : [TypesInformationsGlobalization]?
    let typesInternshipsGlobalization : [TypesInternshipsGlobalization]?
    let typesRolesGlobalization : [TypesRolesGlobalization]?
}
