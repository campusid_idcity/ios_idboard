//
//  TypesEvaluations.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 17/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct TypesEvaluations : Codable {
    let idTypeEvaluation : Int?
    let typeDefaultValue : String?
    let marks : [Marks]?
    let typesEvaluationsGlobalization : [TypesEvaluationsGlobalization]?
}


