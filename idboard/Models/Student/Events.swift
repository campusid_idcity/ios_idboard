//
//  Events.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 18/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct Events : Codable {
    let idEvent: Int?
    let idBusinessEntity : Int?
    let idTypeEvent : Int?
    let idSystemLevel: Int?
    let bbcode : String?
    let date : String?
}
