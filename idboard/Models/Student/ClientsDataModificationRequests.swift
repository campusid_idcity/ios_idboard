//
//  ClientsDataModificationRequests.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 04/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct  ClientsDataModificationRequests : Codable {
    let idRequest : Int?
    let idBusinessEntity : Int?
    let idMessage : Int?
    let idTargetedEntry : Int?
    let targetedTable : String?
    let requestDate : String?
    let metaData : String?
}
