//
//  BusinessEntities.swift
//  idboard
//
//  Created by Marius Marciniak on 20/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct BusinessEntities: Codable {
    let idBusinessEntity: Int?
    let idSalutation: Int?
    let idTypeBusinessEntity: Int?
    let idSystemLevel: Int?
    let idCurrentClass: Int?
    let currentClassName: String?
    let idboard: String?
    let name: String?
    let firstName: String?
    let surName: String?
    let photoPath: String?
    let dateOfBirth: String?
    let placeOfBirth: String?
    let nationality: String?
    let countryOfBirth: String?
    let comments: String?
    let cardTokenId: String?
    let idSalutationNavigation: Salutations?
    let idSystemLevelNavigation: SystemLevels?
    let idTypeBusinessEntityNavigation: TypesBusinessEntities?
    let clientsDataModificationRequests: [ClientsDataModificationRequests]?
    let contactDetails: [ContactDetails]?
    let courses: [Courses]?
    let events: [Events]?
    let informations: [Informations]?
    let internshipConventionRequests: [InternshipConventionRequests]?
    let joinEntityClass: [JoinEntityClass]?
    let joinEntityMessage: [JoinEntityMessage]?
    let logsAlteration: [LogsAlteration]?
    let logsCompetences: [LogsCompetences]?
    let logsPreEnrolment: [LogsPreEnrolment]?
    let marks: [Marks]?
    let roles: [Roles]?
}
