//
//  TypesBusinessEntities.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 04/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct TypesBusinessEntities : Codable {
    let idTypeBusinessEntity : Int?
    let typeDefaultValue : String?
    let businessEntities : [BusinessEntitiesDto]?
    let typesBusinessEntitiesGlobalization : [TypesBusinessEntitiesGlobalization]?
}
