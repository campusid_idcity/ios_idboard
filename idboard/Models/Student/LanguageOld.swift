//
//  LanguageOld.swift
//  idboard
//
//  Created by Marius Marciniak on 20/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct LanguageOld : Codable {
    let ETCSCredits:String?
    let marks :[Note]?
    let status:String
    let name:String
}

struct Note : Codable {
    let matiere:String
    let note:String
    let date:String
    
}
