//
//  MattersDto.swift
//  idboard
//
//  Created by Marius Marciniak on 20/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct MattersDto:Codable{
    let idMatter:Int?
    let idClass:Int?
    let reference:String?
    let descriptionDefaultValue: String?
    let ectscredits:Double?
    let marksNumber: Int?
    let marks:[MarksDto]?
}
