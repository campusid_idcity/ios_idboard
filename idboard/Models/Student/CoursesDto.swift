//
//  CoursesDto.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 18/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct CoursesDto : Codable {
    let idCourse: Int?
    let idClass : Int?
    let dateStart : String?
    let dateEnd: String?
    let teacherName: String?
    let teacherFirstname : String?
    let teacherSurname: String?
    let descriptionDefaultValue: String?
    let typeDefaultValue : String?
    let backGroundColor : String?
    let fontColor: String?
}
