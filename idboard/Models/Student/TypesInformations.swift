//
//  TypesInformations.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 18/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct TypesInformations : Codable {
    let idTypeInformation: Int?
    let typeDefaultValue : String?
    let informations : [Informations]?
    let typesInformationsGlobalization : [TypesInformationsGlobalization]?
}
