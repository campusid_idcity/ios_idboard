//
//  Chapters.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 05/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct Chapters : Codable {
    let idChapter : Int?
    let idMatter : Int?
    let title : String?
    let idMatterNavigation : [Matters]?
    let competences : [Competences]?
}
