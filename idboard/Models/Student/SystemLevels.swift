//
//  SystemLevels.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 18/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct SystemLevels : Codable {
    let idSystemLevel: Int?
    let value : Int?
    let nameDefaultValue : String?
    let adminRights: [AdminRights]?
    let businessEntities : [BusinessEntities]?
    let events: [Events]?
    let informations : [Informations]?
    let systemLevelsGlobalization : [SystemLevelsGlobalization]?
}
