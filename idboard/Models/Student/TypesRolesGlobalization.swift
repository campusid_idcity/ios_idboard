//
//  TypesRolesGlobalization.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 18/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct TypesRolesGlobalization : Codable {
    let idTypesRolesGlobalization: Int?
    let idLanguage : Int?
    let elementValue : String?
    let idTypeRole: Int?
}
