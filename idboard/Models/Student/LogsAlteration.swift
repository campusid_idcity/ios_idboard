//
//  LogsAlteration.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 04/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct  LogsAlteration : Codable {
    let idLogAlteration : Int?
    let idBusinessEntity : Int?
    let idTarget : Int?
    let tableTarget : String?
    let dateTransaction : String?
    let typeTransaction : Int?
}
