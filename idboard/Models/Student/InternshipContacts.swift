//
//  InternshipContacts.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 04/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct  InternshipContacts : Codable {
    let idContact : Int?
    let idSalutation : Int?
    let contactFunction : String?
    let lastName : String?
    let firstName : String?
    let phone : String?
    let fax : String?
    let mail : String?
    let idSalutationNavigation : [Salutations]?
    let internshipConventionRequestsIdRepresentativeNavigation : [InternshipConventionRequests]?
    let internshipConventionRequestsIdTutorNavigation : [InternshipConventionRequests]?
}
