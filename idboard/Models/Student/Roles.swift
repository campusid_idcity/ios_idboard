//
//  Roles.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 18/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct Roles : Codable {
    let idRole: Int?
    let idBusinessEntity : Int?
    let idTypeRole : Int?
    let idSite: Int?
    let idLinkedEntity : Int?
    let dateStart : String?
    let dateEnd : String?
}
