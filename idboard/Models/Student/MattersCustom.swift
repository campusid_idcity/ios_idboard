//
//  MattersCustom.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 18/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct MattersCustom : Codable {
    let idMark : Int?
    let idClass : Int?
    let reference : Int?
    let idTypeEvaluation : Int?
    let descriptionDefaultValue : Double?
    let ectscredits : Double?
    let marks : [MarksCustom]?
}
