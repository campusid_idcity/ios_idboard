//
//  StageAlternance.swift
//  idboard
//
//  Created by Marius Marciniak on 20/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct StageAlternance:Codable {
    let idInternshipOffer:Int?
    let idTypeInternshipOffer: Int?
    let reference:String?
    let title:String?
    let missionSummary:String?
    let companyName:String?
    let duration:String?
    let dateStart:String?
    let dateEnd:String?
    let contact:String?
    let contactPhone:String?
    let contactMail:String?
    let addressOne:String?
    let  addressTwo:String?
    let postalCode:String?
    let city:String?
    let country:String?
    let idTypeInternship:Int?
    let typeDefaultValue:String?
}
