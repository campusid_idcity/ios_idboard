//
//  CompetencesStatus.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 18/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct CompetencesStatus : Codable {
    let idCompetenceStatus : Int?
    let defaultValue : String?
    let statusLevel : Int?
    let competencesStatusGlobalization : [CompetencesStatusGlobalization]?
    let logsCompetences : [LogsCompetences]?
}
