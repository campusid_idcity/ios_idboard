//
//  JoinMatterDomain.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 05/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct JoinMatterDomain : Codable {
    let idJoinMatterDomain : Int?
    let idMatter : Int?
    let idDomain : Int?
}
