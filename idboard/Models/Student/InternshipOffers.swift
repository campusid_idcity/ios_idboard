//
//  InternshipOffers.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 17/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct InternshipOffers : Codable {
    let idInternshipOffer : Int?
    let idTypeInternshipOffer : Int?
    let reference : String?
    let title : String?
    let missionSummary : String?
    let companyName : String?
    let duration : String?
    let dateStart : String?
    let dateEnd : String?
    let contact : String?
    let contactPhone : String?
    let contactMail :String?
    let addressOne : String?
    let addressTwo : String?
    let postalCode : String?
    let city : String?
    let country : String?
    let idTypeInternship : Int?
    let typeDefaultValue : String?
}
