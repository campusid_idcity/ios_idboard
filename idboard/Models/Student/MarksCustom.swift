//
//  MarksCustom.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 18/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

struct MarksCustom : Codable {
    let idMark : Int?
    let idMatter : Int?
    let idClass : Int?
    let idTypeEvaluation : Int?
    let coefficient : Double?
    let value : Double?
    let mediumOfClasses : Double?
    let comments : String?
    let dateCreation : String?
    let isJustifiedAbsence : Bool?
    let idTypeEvaluationNavigation : [TypesEvaluations]?
}
