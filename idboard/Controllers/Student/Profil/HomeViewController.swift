//
//  HomeViewController.swift
//  idboard
//
//  Created by Marius Marciniak on 02/02/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    let Connection_URL = URL(string: "http://idboard.net:9000/student-api/BusinessEntities/2020005")!
    
    
    var myUser = ["Nom : ", "Prénom : ", "Date de naissance : ", "Adresse : ", "Téléphone : ", "Email : "]
    var userInfos = ["Vaz Semedo", "Luis", "03/04", "5 Av. Mathias Duval 06130 Grasse", "0612345678", "2012349@campusid.eu"]
  
    private var userInfos2: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // bouton Save
        let saveButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(profilValiderButtons))
        self.navigationItem.rightBarButtonItem = saveButton
        
        setupTableView()
        setupUserInfos()
    }
    
    // Lorsqu'on appuie sur le bouton Save
    @IBAction func profilValiderButtons(){
        print(userInfos)
        CustomAlert().showAlertReturn(title: "Oups !", message: "Cette fonctionnalité est en cours de développement et arrivera très prochainement.", controller: self, navigationController: nil)
    }

}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if section == 0 {
            return 1
        } else {
            return myUser.count
        }
        
        //return myUser.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Si section 0, on affiche la cellule avec le gradient et l'image utilisateur, sinon on affiche les cellules comportant les informations de l'utilisateur
        switch indexPath.section {
        case 0:
            
            let cell: HomePhotoTableViewCell = tableView.dequeueReusableCell(withIdentifier: "newcellphoto", for: indexPath) as! HomePhotoTableViewCell
            return cell
        default:
            let cell: HomeTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HomeTableViewCell

            cell.ui_profilLabel.text = myUser[indexPath.row]
            cell.ui_valueTextField.text = userInfos[indexPath.row]
            
            // Pour savoir quelle information on modifie
            cell.ui_valueTextField.tag = indexPath.row
            cell.ui_valueTextField.delegate = self
            cell.ui_valueTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)

            return cell
        }
    }
}

extension HomeViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string == "\n" {
            textField.resignFirstResponder()
            self.tableView.reloadData()
            return false
        }
        
        return true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.tableView.reloadData()
    }
    
}

private extension HomeViewController {
    
    private func setupTableView() {
        self.tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    private func setupUserInfos() {
        
        let userConnected: BusinessEntities? = Utility.getUserDefaultsJson(key: UserDefaultsKey.BUSINESS_ENTITIES_CONNECTED, decodingType: BusinessEntities.self)
        
        if let userConnected = userConnected {
            userInfos = []
            userInfos.append(userConnected.name ?? "")
            userInfos.append(userConnected.firstName ?? "")
            
            // On convertit la date au format souhaité
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            
            if let dateOfBirth = userConnected.dateOfBirth {
                guard let dateOfBirthToDate = formatter.date(from: dateOfBirth) else { return }
                
                formatter.dateFormat = "dd/MM/yyyy"
                let finalDateOfBirth = formatter.string(from: dateOfBirthToDate)
                userInfos.append(finalDateOfBirth)
            } else {
                userInfos.append("")
            }
            
            userInfos.append("\(userConnected.countryOfBirth ?? "") - \(userConnected.placeOfBirth ?? "")")
            userInfos.append("")
            userInfos.append("\(userConnected.idboard ?? "")@campusid.eu")
        }
        
        tableView.reloadData()
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        print("textFieldDidChange => \(String(describing: textField.text)) pour l'index \(textField.tag)")
        
        if let texte = textField.text, textField.text != "" {
            self.userInfos[textField.tag] = texte
        }
        
    }
}
