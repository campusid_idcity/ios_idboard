//
//  RedViewController.swift
//  idboard
//
//  Created by Marius Marciniak on 07/02/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import UIKit
import FSCalendar

class PlanningViewController: UIViewController {
   
    @IBOutlet weak var Planning: FSCalendar!
    @IBOutlet weak var TableView: UITableView!
    
    let _customLoader = CustomLoader()
    
    var Tableau_Planning: [Courses] = []
    var Tableau_Course: [Courses] = []
    var Tableau_Date:[String] = []

    fileprivate lazy var dateFormatter2: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        return formatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        configureTabBar()
        configureNavBar()
      
        setupButtonRefresh()
        setupTableview()
        
        fetchData()
    }
    
}

extension PlanningViewController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Tableau_Course.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
     //Creation et configuration d'une cell a un index path donné.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:Planning_TableViewCell  = tableView.dequeueReusableCell(withIdentifier: "Planning_TableViewCell") as! Planning_TableViewCell
        let cellules =  Tableau_Course[indexPath.row]
        
        cell.setCell(course: cellules)
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("j'appuie sur la cellule \(indexPath.row)")
        print("section => \(indexPath.section)")
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
}
extension PlanningViewController : FSCalendarDelegate, FSCalendarDataSource, FSCalendarDelegateAppearance {
    
    fileprivate func createCourses(_ dateFormatter: DateFormatter, _ dateStart: Date?, _ dateEnd: Date?) {
        for course in self.Tableau_Planning
        {
            if let courseDateStart = course.dateStart, let courseDateEnd = course.dateEnd
            {
                
                let formatDateStart = dateFormatter.date(from: String(courseDateStart))
                let formatDateEnd = dateFormatter.date (from: String(courseDateEnd))
                
                //Si la date du cours est compris entre le début du mois et la fin du mois qui arrive
                //alors on l'enregistre dans Tableau_Course et on récupere la date du cours dans Tableau_Date
                if let formatDateStart = formatDateStart, let dateStart = dateStart, let formatDateEnd = formatDateEnd, let dateEnd = dateEnd
                {
                    if formatDateStart  > dateStart && formatDateEnd < dateEnd
                    {
                        self.Tableau_Course.append(course)
                        self.Tableau_Date.append(course.dateStart ?? "error")
                    }
                }
                else
                {
                    print("probleme")
                }
            }
        }
        
        Planning.reloadData()
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        // GetPlanning avec dateStart et dateEnd
        let currentPage = calendar.currentPage
           
        var dateComponentStart = DateComponents()
        dateComponentStart.day = -7
        let dateStart = Calendar.current.date(byAdding: dateComponentStart, to: currentPage)

        var dateComponentEnd = DateComponents()
        dateComponentEnd.month = 2
        let dateEnd = Calendar.current.date(byAdding: dateComponentEnd, to: currentPage)

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"

        self.Tableau_Course.removeAll()
        self.Tableau_Date.removeAll()
                           
                               
        createCourses(dateFormatter, dateStart, dateEnd)
        //TableView.reloadData()

   }
       
   func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
       let formatter = DateFormatter()
       formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
       let myString = formatter.string(from: date)
       
       Tableau_Course.removeAll()
       for course in Tableau_Planning {
          if  course.checkDate(dateDebut: myString) {
           Tableau_Course.append(course)
           }
       }
       TableView.reloadData()
      
   }
    
    //Couleur d'un événement
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]?{
        var Tableau_Color: [UIColor] = []
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let currentDate = formatter.string(from: date)
        
        
        for planning in self.Tableau_Planning{
            
            if planning.dateStart?.prefix(10) == currentDate.prefix(10)
            {
                if let backgroundColor = planning.backGroundColor {
                    Tableau_Color.append(Utility.hexStringToUIColor(hex: backgroundColor))
                }
                
            }
        }
        
        return Tableau_Color.count == 0 ? nil : Tableau_Color

    }

    //Nombre d'évenement dans une journée
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        var compteur = 0
        let dateString = self.dateFormatter2.string(from: date)
        
        for course in self.Tableau_Planning {
            if course.dateStart?.prefix(10) == dateString.prefix(10) {
                 compteur += 1
               
            }
        }
        
        return compteur
        
    }
}

private extension PlanningViewController {
    
    private func configureTabBar() {
        let color = UIColor(red: 219/255.0, green: 13/255.0, blue: 13/255.0, alpha: 1.0)
        self.tabBarController?.moreNavigationController.view.tintColor = color
    }
    
    private func configureNavBar() {
        if #available(iOS 11.0, *) {
            //navigationController?.navigationBar.prefersLargeTitles = true
        }
    }
    
    private func setupTableview() {
        TableView.delegate = self
        TableView.dataSource = self
    }
    
    private func fetchData() {
        self._customLoader.setupActivityIndicator(controller: self)
        PlanningService.getPlannings { (result) in
            DispatchQueue.main.async {
                switch result {
                case .failure(let err):
                    print("Failure => \(err) ")
                    
                case .success(let plannings):
                    self.Tableau_Planning = plannings
                    self.Tableau_Course = []
                    
                    // On récupère la date selectionner. Si on charge la page, on récupère la date d'aujourd'hui
                    let selectedDate: Date = self.Planning.selectedDate ?? Date()
                    let cal = Calendar(identifier: .gregorian)
                    let newDate = cal.startOfDay(for: selectedDate)
                    let endToday = cal.startOfDay(for: selectedDate).addingTimeInterval( (60*23) * 60)
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                    
                    // On créé la liste des cours à la date d'aujourdhui ou celle selectionne sur le calendrier
                    self.createCourses(formatter, newDate, endToday)
                    
                    self.TableView.reloadData()
                    self.Planning.reloadData()
                }
                self._customLoader.hideActivityIndicator(controller: self)
            }
        }
    }
    
    func setupButtonRefresh() {
        let image = UIImage(named: "refresh")
        let rightButton = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(self.actionButtonRefresh))
        self.navigationItem.rightBarButtonItem = rightButton
    }
    
    @objc func actionButtonRefresh() {
        self.fetchData()
    }
}
