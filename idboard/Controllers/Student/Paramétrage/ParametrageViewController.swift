//
//  ParametrageViewController.swift
//  idboard
//
//  Created by Martin on 24/04/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import UIKit

class ParametrageViewController: UIViewController {
    var strings = ["Déconnexion"]
    @IBOutlet weak var ui_tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension ParametrageViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return strings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: ParametrageTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ParametrageTableViewCell", for: indexPath) as! ParametrageTableViewCell
        cell.ui_label.text = strings[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Si on appuie sur la premiere ligne (se deconnecter), on supprime le cache du téléphone et on revient sur la page d'accueil
        if indexPath.row == 0 {
            let alert = UIAlertController(title: "Déconnexion", message: "Voulez vous vraiment vous déconnecter ?", preferredStyle: .alert)

                 let ok = UIAlertAction(title: "Se déconnecter", style: .default, handler: { action in
                    if let bundle = Bundle.main.bundleIdentifier {
                        UserDefaults.standard.removePersistentDomain(forName: bundle)
                        UserDefaults.standard.synchronize()
                    }
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let homePage: UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController")
                    //Set the current root controller and add the animation with a UIView transition
                    guard let window = self.view.window else { return }
                    window.rootViewController = homePage
                    UIView.transition(with: window,
                                      duration: 0.6,
                                      options: .transitionFlipFromLeft,
                                      animations: { window.rootViewController = homePage } ,
                                      completion: nil)
                    
                    self.present(homePage, animated: true, completion: nil)
                 })
                 alert.addAction(ok)
                let cancel = UIAlertAction(title: "Annuler", style: .cancel, handler: nil)
                 alert.addAction(cancel)
                 self.present(alert, animated: true)

        }
    }
    
    
}
