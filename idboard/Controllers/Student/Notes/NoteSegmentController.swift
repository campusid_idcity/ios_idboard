//
//  NoteSegmentController.swift
//  idboard
//
//  Created by Marius Marciniak on 10/04/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import UIKit

class NoteSegmentController: UISegmentedControl {

    @IBAction func recurranceChanged(sender: UISegmentedControl) {
               
        print ("index: ", sender.selectedSegmentIndex)
    
        if sender.selectedSegmentIndex == 0 {
            print("No ")
        }
    
        if sender.selectedSegmentIndex == 1 {
            print("Sometimes ")
    
        }
    
    
        if sender.selectedSegmentIndex == 2 {
            print("Yes")
    
        }
    
    }


}
