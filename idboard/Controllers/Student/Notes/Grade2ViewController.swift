//
//  MarkViewController.swift
//  idboard
//
//  Created by Marius Marciniak on 06/03/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import UIKit

class Grade2ViewController: UIViewController{
    
    @IBOutlet weak var ui_segmentedControl: NoteSegmentController!
    @IBOutlet weak var ui_tableView: UITableView!
    
    @IBOutlet weak var pickerView: UIPickerView!
    
    let _customLoader = CustomLoader()
    
    var pickerData: [String] = [String]()
    
    var _matieres: [DomainDto] = []
    var _matieresSaved: [DomainDto] = []
    
    var _notes: [MarksDto] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupButtonRefresh()
        self.setupPickerView()
        self.setupTableview()
        self.fetchData()
    }
    
    // Redirection vers un autre controlleur en fonction de l'identifiant d'une Segue
    // Ici, on récupère les notes d'une matière et on les transmet au controlleur de type GradeDetailViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetailNoteFromNotes" {
            if let cell: NoteTableViewCell = sender as? NoteTableViewCell {
                if let indexPath = self.ui_tableView.indexPath(for: cell) {
                    if let selectedMatter: MattersDto = self._matieres[indexPath.section].matters?[indexPath.row], let marks = selectedMatter.marks, marks.count > 0 {
                        let gradeDetailViewController: GradeDetailViewController = segue.destination as! GradeDetailViewController
                        gradeDetailViewController._matter = selectedMatter
                    }
                }
            }
        }
    }

}

extension Grade2ViewController: UITableViewDelegate, UITableViewDataSource {

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return _matieres.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _matieres[section].matters?.count ?? 0
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:NoteTableViewCell  = tableView.dequeueReusableCell(withIdentifier: "NoteTableViewCell") as! NoteTableViewCell

        let matiere =  _matieres[indexPath.section].matters?[indexPath.row]

        cell.display(matter: matiere)

        return cell
     }
        
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    // On créé une section par domaine. On y retrouvera le nom du domaine + le nombre de crédit ECTS
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let headerView = UIView(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 60))
        headerView.backgroundColor = UIColor(red: 178 / 255, green: 34 / 255, blue: 34 / 255, alpha: 1.0)
        
        // On change la couleur de fond en fonction du statut du domaine.
        // Gris => en cours de validation | Vert => validée | Rouge => non validé
        
        switch self._matieres[section].isValidate {
        case 0:
            headerView.backgroundColor = UIColor(red: 170 / 255, green: 170 / 255, blue: 170 / 255, alpha: 1.0)
        case 1:
            headerView.backgroundColor = UIColor(red: 76 / 255, green: 175 / 255, blue: 80 / 255, alpha: 1.0)
        default:
            headerView.backgroundColor = UIColor(red: 178 / 255, green: 34 / 255, blue: 34 / 255, alpha: 1.0)
        }
        
        let labelMatiere = UILabel()
        labelMatiere.frame = CGRect(x: 15, y: 0 , width: headerView.frame.width-10 - 150, height: headerView.frame.height)
        labelMatiere.text = _matieres[section].descriptionDefaultValueDomain
        labelMatiere.textColor = UIColor.white
        
        let labelCredit = UILabel()
        labelCredit.frame = CGRect(x: 0, y: 0, width: headerView.frame.width-10, height: headerView.frame.height)
        labelCredit.textAlignment = NSTextAlignment.right
        labelCredit.textColor = UIColor.white
        labelCredit.text = "Crédits ECTS : \(String(format: "%1.f", _matieres[section].ectscredits ?? 0) )".replacingOccurrences(of: ".", with: ",")
        
        headerView.addSubview(labelMatiere)
        headerView.addSubview(labelCredit)

        return headerView
    }
    
}

extension Grade2ViewController:UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
         print("row select \(row)")
        
    }

}

extension Grade2ViewController {
    
    private func setupPickerView() {
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
              
        pickerData = ["B1", "B2", "B3", "M1", "M2"]
        
        let user: BusinessEntities? = Utility.getUserDefaultsJson(key: UserDefaultsKey.BUSINESS_ENTITIES_CONNECTED, decodingType: BusinessEntities.self)
        
        // On récupère la classe de l'utilisateur connecté afin de configurer par défaut le Picker View sur cette classe
        if let user = user, let currentClassName = user.currentClassName {
            guard let index = pickerData.firstIndex(of: String(currentClassName.prefix(2))) else { return }
            self.pickerView.selectRow(index, inComponent: 0, animated: false)
        }
    }
    
    private func setupTableview() {
        self.ui_tableView.tableFooterView = UIView()
    }
    
    private func fetchData() {
        ui_segmentedControl.addTarget(self, action: #selector(checkIndexOfSegmentedUi), for: .valueChanged)
        
        self._customLoader.setupActivityIndicator(controller: self)
        LanguageService.getDomaines { (result) in
            DispatchQueue.main.async {
                switch result {
                case .failure:
                    CustomAlert().showAlertReturn(title: "Oups !", message: "Une erreur est survenue lors de la récupération du relevé de notes, veuillez réessayer.", controller: self, navigationController: self.navigationController)
                case .success(let domaines):
                    
                    /* Algo de tri lorsqu'on recupère les notes sur toutes les années confondues */
                    /*
                    let userConnected: BusinessEntities? = Utility.getUserDefaultsJson(key: UserDefaultsKey.BUSINESS_ENTITIES_CONNECTED, decodingType: BusinessEntities.self)
    
                    if let userConnected = userConnected, let currentClass = userConnected.idCurrentClass {
                        self._matieres = domaines.filter {
                            ($0.matters?.contains(where: { (matter) -> Bool in
                                matter.idClass == currentClass
                            }) ?? false)
                        }
                        self._matieresSaved = self._matieres
                        
                    }
                    */
                    
                    self._matieres = domaines
                    self._matieresSaved = domaines
        
                    self.ui_tableView.reloadData()
                }
                self._customLoader.hideActivityIndicator(controller: self)
            }
        }
    }
    
    func setupButtonRefresh() {
        let image = UIImage(named: "refresh")
        let rightButton = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(self.actionButtonRefresh))
        self.navigationItem.rightBarButtonItem = rightButton
    }
    
    @objc func actionButtonRefresh() {
        self.fetchData()
    }
    
    @objc private func checkIndexOfSegmentedUi() {
        
        // Si des matieres existent
        if self._matieres.count > 0 {
            
            switch ui_segmentedControl.selectedSegmentIndex {
            case 1:
                self._matieres = self._matieresSaved.filter { $0.isValidate == 0}
            case 2:
                self._matieres = self._matieresSaved.filter { $0.isValidate == 1}
            case 3:
                self._matieres = self._matieresSaved.filter { $0.isValidate == -1}
            default:
                self._matieres = self._matieresSaved
            }
            
            self.ui_tableView.reloadData()
        }
    }
}
