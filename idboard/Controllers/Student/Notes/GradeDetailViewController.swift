//
//  GradeDetailViewController.swift
//  idboard
//
//  Created by Marius Marciniak on 18/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import UIKit

class GradeDetailViewController: UIViewController {

    @IBOutlet weak var TableView: UITableView!
    
    var _matter: MattersDto? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        TableView.reloadData()
    }
    


}

extension GradeDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self._matter?.marks?.count ?? 0
            
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:GradeDetailTableTableViewCell  = tableView.dequeueReusableCell(withIdentifier: "GradeDetailTableTableViewCell") as! GradeDetailTableTableViewCell
        
        if let matter = _matter {
            let mark: MarksDto? = matter.marks?[indexPath.row]
            
            cell.display(matter: matter, mark: mark)
        }

        return cell
    }

    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    
}
