//
//  TrombinoscopeViewController.swift
//  idboard
//
//  Created by CampusID002 on 11/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import UIKit

class TrombinoscopeViewController: UIViewController {

    @IBOutlet weak var ui_tableView: UITableView!
    
    let _customLoader = CustomLoader()
    
    var _trombinoscope: TrombinoscopeDto? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupButtonRefresh()
        self.fetchData()
    }


}
extension TrombinoscopeViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self._trombinoscope?.students?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell: TrombinscopeFirstCell_TableViewCell = tableView.dequeueReusableCell(withIdentifier: "TrombinscopeFirstCell_TableViewCell", for: indexPath) as! TrombinscopeFirstCell_TableViewCell
            
            cell.display(trombi: _trombinoscope)
            
            return cell
        } else {
            let cell: Trombinoscope_TableViewCell = tableView.dequeueReusableCell(withIdentifier: "Trombinoscope_TableViewCell", for: indexPath) as!  Trombinoscope_TableViewCell
            
            cell.display(student: _trombinoscope?.students?[indexPath.row-1])
            return cell
        }

    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
}

extension TrombinoscopeViewController {
    
    private func fetchData() {
        
        self._customLoader.setupActivityIndicator(controller: self)
        TrombinoscopeService.getTrombinoscope { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(let trombinoscope):
                    
                    print("trombinoscope => \(trombinoscope)")
                    self._trombinoscope = trombinoscope
                    self.ui_tableView.reloadData()
                case .failure(let error):
                    print("Failure => \(error) ")
                    
                    let alert = UIAlertController(title: "Oups !", message: "Une erreur est survenue lors de la récupération du trombinoscope, veuillez réessayer.", preferredStyle: .alert)
                    let cancel = UIAlertAction(title: "OK", style: .default, handler: nil)
                    alert.addAction(cancel)
                    self.present(alert, animated: true)
                }
                self._customLoader.hideActivityIndicator(controller: self)
            }
        }
    }
    
    func setupButtonRefresh() {
        let image = UIImage(named: "refresh")
        let rightButton = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(self.actionButtonRefresh))
        self.navigationItem.rightBarButtonItem = rightButton
    }
    
    @objc func actionButtonRefresh() {
        self.fetchData()
    }
}
