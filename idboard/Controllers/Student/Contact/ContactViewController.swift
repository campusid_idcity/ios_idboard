//
//  ContatcViewController.swift
//  idboard
//
//  Created by CampusID001 on 24/04/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import UIKit
import MessageUI

class ContactViewController: UIViewController {
   // ContactTextField
    override func viewDidLoad() {
          super.viewDidLoad()

          if #available(iOS 13.0, *) {
             //overrideUserInterfaceStyle = .dark
    
         } else {
             
         }
      }
    
    //Fonction permettant d'appeler directement l'école
    @IBAction func Call(_ sender: Any) {
        if let url = URL(string: "tel://0984325101"),
        UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)

        }
    }
    
    @IBAction func Mail(_ sender: Any) {
        
        showMailComposer()
            
    }
    
    //Fonction permettant d'envoyer un mail avec comme destinater scolarite@eid.eu
    //Renvoie une pop-up d'erreur si l'envoie du mail ne c'est pas fait
    func showMailComposer(){
        guard MFMailComposeViewController.canSendMail()else{
            showPopUp()
            print("Erreur envoie")
            return
        }
        let composer = MFMailComposeViewController()
        composer.mailComposeDelegate = self
        composer.setToRecipients(["scolarite@eid.eu"])
        composer.setSubject("contacter")
        //composer.setMessageBody(" ", isHTML: false)
        
        present(composer, animated: true)
        print("Envoie du mail")
    }
    
    //Creation d'une pop-up d'erreur pour informer qu'il est impossible d'envoyer de mail (messagerie mail indisponible)
    func showPopUp(){
        let alert = UIAlertController(title: "Mail Indisponible", message: "Il semblerait qu'aucun client de messagerie ne soit configuré sur votre appareil.", preferredStyle: .alert)
         let action = UIAlertAction(title: "Fermer", style: .cancel, handler: nil)
       
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
     

}

//Creation de message permettant d'indiquer si le mail a étais envoyer ou pas, de permettre de quitter l'application et d'enregistrer .
extension ContactViewController: MFMailComposeViewControllerDelegate{
    public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        if let _ = error{
            controller.dismiss(animated: true)
        }
        
        switch result{
            case .cancelled:
                print("Cancelled")
            case .failed:
                print("Failed to send")
            case .saved:
                print("Saved")
            case .sent:
                print("Email send")
        default:
            print("Default send")
    }
        controller.dismiss(animated: true)
    
    }
}
