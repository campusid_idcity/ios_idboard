//  MessageViewController.swift
//  idboard
//
//  Created by CampusID001 on 13/03/2020.
//  Copyright © 2020 CampusID. All rights reserved.

import UIKit


class MessageViewController: UIViewController {
    
    let _customLoader = CustomLoader()
    let sectionsTitles = ["Anniversaires" , "Autres"]

    @IBOutlet weak var ui_TableView: UITableView!
    var messages: [Message] = []
    var birthdays: [BirthdayDateDto] = []
        
    override func viewDidLoad(){
        super.viewDidLoad()
            
        self.fetchData()
        self.setupButtonRefresh()
    }
    
}

//Creation de l'interface des Messages
//permet de créer automatiquement une nouvelle cellule
extension MessageViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionsTitles[section]
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        switch section {
        case 0:
            return birthdays.count
        default:
            return messages.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    //Creation d'une cellule selon si c'est un "anniversaire" ou une "interface"
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell: MessageBirthdayTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MessageBirthdayTableViewCell") as! MessageBirthdayTableViewCell
            
            let birthday: BirthdayDateDto = birthdays[indexPath.row]
            cell.display(birthday: birthday)
            
            return cell
        default:
            let cell: MessageTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MessageTableViewCell") as! MessageTableViewCell
            
            let message: Message = messages[indexPath.row]
            cell.display(message: message)
            
            return cell
        }
    }
    
}

extension MessageViewController {
    
    private func fetchData() {
        self._customLoader.setupActivityIndicator(controller: self)
        MessageService.getMessage { (result) in
            DispatchQueue.main.async {
                switch result {
                case .failure(let err):
                    print("Failure => \(err) ")
                    // On affiche une erreur sur l'écran => (UIAlertController)
                    CustomAlert().showAlertReturn(title: "Oups !", message: "Une erreur est survenue lors de la récupération des messages, veuillez réessayer.", controller: self, navigationController: self.navigationController)
                case .success(let messages):
                    print("liste des articles => \(messages)")
                    // On affiche cette liste d'articles sur l'écran
                    self.messages = messages
                    self.ui_TableView.reloadData()
                }
                self._customLoader.hideActivityIndicator(controller: self)
            }
        }
        
        MessageService.getBirthday { (result) in
            DispatchQueue.main.async {
                switch result {
                case .failure(let err):
                    print("Failure => \(err) ")
                case .success(let birthdays):
                    print("liste des birthdays => \(birthdays)")
                    // On affiche cette liste d'articles sur l'écran
                    self.birthdays = birthdays
                    self.ui_TableView.reloadData()
                }
            }
        }
    }
    
    private func setupButtonRefresh() {
        let image = UIImage(named: "refresh")
        let rightButton = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(self.actionButtonRefresh))
        self.navigationItem.rightBarButtonItem = rightButton
    }
    
    @objc func actionButtonRefresh() {
        self.fetchData()
    }
}
