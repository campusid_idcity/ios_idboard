//
//  StageAlternanceDetailViewController.swift
//  idboard
//
//  Created by Marius Marciniak on 21/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import UIKit

class StageAlternanceDetailViewController: UIViewController {
    
    @IBOutlet weak var ui_tableView: UITableView!
    
    var _stage: StageAlternance? = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = _stage?.title ?? "Détails"
    }

}

extension StageAlternanceDetailViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _stage != nil ? 1 : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: StageAlternanceDetailTableViewCell = tableView.dequeueReusableCell(withIdentifier: "StageAlternanceDetailTableViewCell", for: indexPath) as! StageAlternanceDetailTableViewCell
        
        cell.display(stageAlternance: _stage)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    
}
