//
//  StageAlternanceViewController.swift
//  idboard
//
//  Created by Marius Marciniak on 11/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import UIKit

class StageAlternanceViewController: UIViewController {

    @IBOutlet weak var StageAlternanceTableView: UITableView!
    
    let _customLoader = CustomLoader()
    
    var Tableau_StageAlternance:[StageAlternance] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        StageAlternanceTableView.delegate = self
        StageAlternanceTableView.dataSource = self
        
        self.setupButtonRefresh()
        self.fetchData()
    }
    
    // Redirection vers un autre controlleur en fonction de l'identifiant d'une Segue
    // Ici, on récupère les notes d'une matière et on les transmet au controlleur de type GradeDetailViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetailStageAlternanceFromStageAlternance" {
            if let cell: StageAlternanceTableViewCell = sender as? StageAlternanceTableViewCell {
                if let indexPath = self.StageAlternanceTableView.indexPath(for: cell) {
                    if let selectedStage: StageAlternance = self.Tableau_StageAlternance[indexPath.row] {
                        let stageAlternanceDetailViewController: StageAlternanceDetailViewController = segue.destination as! StageAlternanceDetailViewController
                        stageAlternanceDetailViewController._stage = selectedStage
                    }
                }
            }
        }
    }

}

extension StageAlternanceViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Tableau_StageAlternance.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    //Creation et configuration d'une cell a un index path donné.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:StageAlternanceTableViewCell  = tableView.dequeueReusableCell(withIdentifier: "StageAlternanceTableViewCell") as! StageAlternanceTableViewCell
        
        let cellules =  Tableau_StageAlternance[indexPath.row]
          
        cell.setCell(nom: cellules.companyName ?? "", contrat: cellules.typeDefaultValue ?? "", duration: cellules.duration ?? "", language: cellules.title ?? "")

        return cell
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    

}

extension StageAlternanceViewController {
    
    private func fetchData() {
        self._customLoader.setupActivityIndicator(controller: self)
        StageAlternanceService.getStageAlternance { (result) in
            DispatchQueue.main.async {
                switch result {
                    case .failure(let err):
                        print("Failure => \(err) ")
                          
                    case .success(let offres):
                        self.Tableau_StageAlternance = offres
                        self.StageAlternanceTableView.reloadData()
                      
                }
                self._customLoader.hideActivityIndicator(controller: self)
            }
        }
    }
    
    func setupButtonRefresh() {
        let image = UIImage(named: "refresh")
        let rightButton = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(self.actionButtonRefresh))
        self.navigationItem.rightBarButtonItem = rightButton
    }
    
    @objc func actionButtonRefresh() {
        self.fetchData()
    }
}
