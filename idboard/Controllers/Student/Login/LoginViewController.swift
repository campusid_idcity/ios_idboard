//
//  LoginViewController.swift
//  idboard
//
//  Created by CampusID002 on 07/02/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var ui_loginTextField: UITextField!
    @IBOutlet weak var ui_passwordTextField: UITextField!
    @IBOutlet weak var ui_button: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ui_passwordTextField.attributedPlaceholder = NSAttributedString(string: "Mot de passe", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        ui_loginTextField.attributedPlaceholder = NSAttributedString(string: "Numéro IDBoard", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        ui_loginTextField.alpha = 0.4;
        ui_passwordTextField.alpha = 0.4;
        //ui_loginTextField.leftViewMode = UITextField.ViewMode.always
        //ui_loginTextField.leftViewMode = .always
        
        //let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 5, height: 5))
        // let image = UIImage(named: "icon_lock")
        //imageView.image = image
        //ui_loginTextField.leftView = imageView
        self.ui_button.layer.cornerRadius = 10
        
        let colors_1 : UIColor = UIColor(red: 201/255, green: 70/255, blue: 61/255, alpha: 1)
        let colors_2 : UIColor = UIColor(red: 38/255, green: 7/255, blue: 26/255, alpha: 1)
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [colors_1.cgColor, colors_2.cgColor]
        self.view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    
    @IBAction func ResetButton(_ sender: Any) {
        let user :String? = ui_loginTextField.text ?? ""
        if(user == "" )
        {
            let alert = UIAlertController(title: "Champs non remplis", message: "Veuillez renseigner votre numéro IDBoard pour pouvoir faire une demande de changement de mot de passe", preferredStyle: .alert)
            let cancel = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(cancel)
            self.present(alert, animated: true)
            return
        }else{
            self.showSpinner()
            LoginService.resetPsw(idboard: user!, completion:  { (result) in
                DispatchQueue.main.async {
                    switch result {
                    case .success(let reset):
                        if reset?.isExist == true{
                            //SendMail(idboard)
                            print("ok ca fonctionne")
                        }else{
                            let alert = UIAlertController(title: "Numéro IDBoard non valide", message: "Il semblerait que votre numéro IDBoard ne soit pas valide", preferredStyle: .alert)
                            let cancel = UIAlertAction(title: "OK", style: .default, handler: nil)
                            alert.addAction(cancel)
                            self.present(alert, animated: true)
                        }
                        
                        
                    case .failure(let error):
                        print("Failure => \(error) ")
                        let alert = UIAlertController(title: "Erreur API", message: "Une erreur est survenue, veuillez réessayer plus tard ou contacter l'administration", preferredStyle: .alert)
                        let cancel = UIAlertAction(title: "OK", style: .default, handler: nil)
                        alert.addAction(cancel)
                        self.present(alert, animated: true)
                    }
                    self.removeSpinner()
                }
            })
        }
    }
    
    @IBAction func LoginButton(_ sender: Any){
        
        let user = ui_loginTextField.text
        let psw = ui_passwordTextField.text
        
        if(user == "" || psw == "")
        {
            let alert = UIAlertController(title: "Champs non remplis", message: "Veuillez renseigner votre numéro IDBoard ainsi que votre mot de passe", preferredStyle: .alert)
            let cancel = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(cancel)
            self.present(alert, animated: true)
            return
        }
        else
        {
            if let user = user, let pswChecked = psw {
                DoLogin(user, pswChecked)
            }
            else{
                
            }
        }
    }
    
    func DoLogin(_ user:String, _ psw:String)
    {
        self.showSpinner()
        LoginService.login(user: user, psw: psw) { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success:
                    
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    
                    let homePage: UIViewController = storyBoard.instantiateViewController(withIdentifier: "mainPage")
                    homePage.modalTransitionStyle = .crossDissolve
                    
                    self.present(homePage, animated: true, completion: nil)
                    
                case .failure(let error):
                    print("Failure => \(error) ")
                    let alert = UIAlertController(title: "Oups !", message: "Identifiant ou mot de passe incorrect, veuillez réessayer. Si le problème persiste, veuillez contacter l'administration.", preferredStyle: .alert)
                    let cancel = UIAlertAction(title: "OK", style: .default, handler: nil)
                    alert.addAction(cancel)
                    self.present(alert, animated: true)
                }
                self.removeSpinner()
            }
            
        }
    }
}
