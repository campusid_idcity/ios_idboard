//
//  GradientView.swift
//  idboard
//
//  Created by Martin on 03/04/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation
import UIKit

class GradientView: UIView{
    var FirstColor: UIColor = UIColor.clear{
        didSet{
            updateView()
        }
    }
    
    var SecondColor: UIColor = UIColor.clear{
        didSet{
            updateView()
        }
    }
    override class var layerClass: AnyClass{
        get{
            return CAGradientLayer.self
        }
    }
    
    func updateView(){
        let layer = self.layer as!CAGradientLayer
        layer.colors = [FirstColor.cgColor, SecondColor]
        
    }
}
