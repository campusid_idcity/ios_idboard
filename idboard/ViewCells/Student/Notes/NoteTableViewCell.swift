//
//  NoteTableViewCell.swift
//  idboard
//
//  Created by Marius Marciniak on 10/04/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import UIKit

class NoteTableViewCell: UITableViewCell {

    @IBOutlet weak var MatiereLabel: UILabel!
    @IBOutlet weak var NoteLabel: UILabel!
    @IBOutlet weak var CreditECTSLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    func setCell(matiere:String, note:String, date:String)
    {
        self.MatiereLabel.text = matiere
        self.NoteLabel.text = note
        self.CreditECTSLabel.text = date
    }
    
    func display(matter: MattersDto?) {
        if let matter = matter {
            self.MatiereLabel.text = matter.descriptionDefaultValue
            if let marks = matter.marks {
                var notes: String = ""
                
                for (index,mark) in marks.enumerated() {
                    if index == marks.count - 1 {
                        notes += "\(String(format: "%.1f", mark.value ?? 0))".replacingOccurrences(of: ".", with: ",")
                    } else {
                        notes += "\(String(format: "%.1f", mark.value ?? 0)) - ".replacingOccurrences(of: ".", with: ",")
                    }
                }
                
                self.NoteLabel.text = "Note(s) obtenue(s) : \(notes)"
                //self.CreditECTSLabel.text = "ECTS : \(matter.ectscredits ?? 0)"
                self.CreditECTSLabel.text = ""
            } else {
                self.NoteLabel.text = "Aucune note disponible"
                self.CreditECTSLabel.text = ""
            }
            
        }
    }
}
