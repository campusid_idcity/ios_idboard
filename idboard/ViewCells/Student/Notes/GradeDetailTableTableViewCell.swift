//
//  GradeDetailTableTableViewCell.swift
//  idboard
//
//  Created by Marius Marciniak on 18/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import UIKit

class GradeDetailTableTableViewCell: UITableViewCell {

    @IBOutlet weak var NomMatiere: UILabel!
    @IBOutlet weak var Note: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func display(matter: MattersDto, mark: MarksDto?) {
        
        if let mark = mark
        {
            self.NomMatiere.text = mark.idTypeEvaluationNavigation?.typeDefaultValue
            self.Note.text =  "\(String(format: "%.1f", mark.value ?? 0))".replacingOccurrences(of: ".", with: ",")
        }
            
    }

}
