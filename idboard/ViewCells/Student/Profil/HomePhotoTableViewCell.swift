//
//  HomePhotoTableViewCell.swift
//  idboard
//
//  Created by Luis Miguel Vaz Semedo on 15/05/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import UIKit

class HomePhotoTableViewCell: UITableViewCell {
    
    let gradientLayer = CAGradientLayer()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        // pour modifier le background
        let colors_1 : UIColor = UIColor(red: 201/255, green: 70/255, blue: 61/255, alpha: 1)
        let colors_2 : UIColor = UIColor(red: 38/255, green: 7/255, blue: 26/255, alpha: 1)
               
        gradientLayer.frame = self.contentView.bounds
        gradientLayer.colors = [colors_1.cgColor, colors_2.cgColor]
        layer.insertSublayer(gradientLayer, at: 0)
        
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        gradientLayer.frame = bounds
    }


}
