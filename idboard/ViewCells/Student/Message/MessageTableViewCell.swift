//
//  MessageTableViewCell.swift
//  idboard
//
//  Created by CampusID001 on 27/03/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import UIKit

class MessageTableViewCell: UITableViewCell {

    @IBOutlet weak var ui_messageLabel: UILabel!
    @IBOutlet weak var ui_dateLabel: UILabel!
    @IBOutlet weak var ui_photoImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func display(message: Message?) {
        
        if let message = message, let dateStart = message.dateStart {
            
            ui_messageLabel.text = message.title
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            
            guard let date: Date = formatter.date(from: dateStart) else { return }
            formatter.dateFormat = "dd/MM/yyyy"
            
            let dateStartFinal = formatter.string(from: date)
            ui_dateLabel.text = dateStartFinal
            
        }
    }

}
