//
//  MessageBirthdayTableViewCell.swift
//  idboard
//
//  Created by Marius Marciniak on 25/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import UIKit

class MessageBirthdayTableViewCell: UITableViewCell {

    @IBOutlet weak var ui_nameLabel: UILabel!
    @IBOutlet weak var ui_messageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func display(birthday: BirthdayDateDto) {
        ui_nameLabel.text = birthday.dateOfToday ?? ""
        ui_messageLabel.text = birthday.birthdaySentence ?? ""
    }
    

}
