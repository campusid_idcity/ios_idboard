//
//  StageAlternanceTableViewCell.swift
//  idboard
//
//  Created by Marius Marciniak on 11/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import UIKit

class StageAlternanceTableViewCell: UITableViewCell {

    @IBOutlet weak var NomEntreprise: UILabel!
    @IBOutlet weak var TypeContrat: UILabel!
    @IBOutlet weak var Duration: UILabel!
    @IBOutlet weak var Language: UILabel!
    
    @IBOutlet weak var LogoEntreprise: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let image = UIImage(named: "logo-campusid-1.png")
       // self.LogoEntreprise(image)
        self.LogoEntreprise.image = image
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(nom:String, contrat:String, duration:String, language:String)
     {
         self.NomEntreprise.text = nom
         self.TypeContrat.text = contrat
         self.Duration.text = duration
         self.Language.text = language

     }

}
