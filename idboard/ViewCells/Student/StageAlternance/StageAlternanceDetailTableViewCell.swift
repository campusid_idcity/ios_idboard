//
//  StageAlternanceDetailTableViewCell.swift
//  idboard
//
//  Created by Marius Marciniak on 21/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import UIKit

class StageAlternanceDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var companyName: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var missionSummary: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func display(stageAlternance: StageAlternance?) {
        if let stageAlternance = stageAlternance {
            companyName.text = stageAlternance.companyName ?? ""
            title.text = stageAlternance.title ?? ""
            missionSummary.text = stageAlternance.missionSummary ?? ""
        }
    }

}
