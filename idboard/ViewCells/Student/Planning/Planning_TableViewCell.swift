//
//  Planning_TableViewCell.swift
//  idboard
//
//  Created by Marius Marciniak on 20/03/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import UIKit

class Planning_TableViewCell: UITableViewCell {
   
    @IBOutlet weak var matiere: UILabel!
    @IBOutlet weak var horaire: UILabel!
    @IBOutlet weak var professeur: UILabel!
    
    @IBOutlet weak var ViewHoraire: UIView!
    
    @IBOutlet weak var LabelPoint: UILabel!
  
    override func awakeFromNib() {
        super.awakeFromNib()
   
        ViewHoraire.layer.cornerRadius = 5
        LabelPoint.layer.cornerRadius = 7.5
        LabelPoint.layer.masksToBounds = true
        self.separatorInset.left = 40
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setCell(course:Courses)
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        if let dateStart = course.dateStart, let dateEnd = course.dateEnd {
            guard let dateStartToDate = formatter.date(from: dateStart) else { return }
            guard let dateEndToDate = formatter.date(from: dateEnd) else { return }
            
            formatter.dateFormat = "HH:mm"
            let finalDateStart = formatter.string(from: dateStartToDate)
            let finalDateEnd = formatter.string(from: dateEndToDate)
            
            self.horaire.text = "\(finalDateStart) - \(finalDateEnd)"
        }
        
        self.matiere.text = course.descriptionDefaultValue
        self.professeur.text = course.teacherName        
        
        self.colorHoraire(backgroundColor: course.backGroundColor)
    }
    
    func colorHoraire(backgroundColor: String?)
    {
        if let backgroundColor = backgroundColor {
            ViewHoraire.backgroundColor = Utility.hexStringToUIColor(hex: backgroundColor)
            LabelPoint.backgroundColor = Utility.hexStringToUIColor(hex: backgroundColor)
        } else {
            ViewHoraire.backgroundColor = UIColor.orange
            LabelPoint.backgroundColor = UIColor.orange
        }
    }

}
