//
//  TrombinscopeFirstCell_TableViewCell.swift
//  idboard
//
//  Created by CampusID002 on 12/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import UIKit

class TrombinscopeFirstCell_TableViewCell: UITableViewCell {

    @IBOutlet weak var LabelYear: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func display(trombi : TrombinoscopeDto?){
        if let trombi = trombi {
            LabelYear.text = (trombi.name ?? "") + " " + (trombi.scholarYear ?? "")
        }
    }

}
