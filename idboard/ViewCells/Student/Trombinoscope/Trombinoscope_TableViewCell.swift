//
//  Trombinoscope_TableViewCell.swift
//  idboard
//
//  Created by CampusID002 on 12/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import UIKit

class Trombinoscope_TableViewCell: UITableViewCell {
    
    @IBOutlet weak var ui_idboard: UILabel!
    @IBOutlet weak var ui_name: UILabel!
    @IBOutlet weak var Photo: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func display(student : BusinessEntities?){
        if let student = student {
            setImage(from: student.photoPath ?? "")
            ui_idboard.text = student.idboard ?? ""
            ui_name.text = "\(student.name ?? "") - \(student.firstName ?? "")"
        }
       
    }
    func setImage(from url: String) {
        guard let imageURL = URL(string: url) else { return }
    
            // just not to cause a deadlock in UI!
        DispatchQueue.global().async {
            let imageData = try? Data(contentsOf: imageURL)
            
            if let imageData = imageData {
                let image = UIImage(data: imageData)
               DispatchQueue.main.async {
                   self.Photo?.image = image
               }
            } else {
                DispatchQueue.main.async {
                    self.Photo.image = UIImage(named: "trombinoscope_default")
                }
            }
           
        }
    }

}
