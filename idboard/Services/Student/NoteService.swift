//
//  Matieres-Notes.swift
//  idboard
//
//  Created by Marius Marciniak on 10/04/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

enum LanguageError: Error {
    case noDataAvailable
    case cannotProcessData
    //case noHttpResponse
}

class LanguageService {
    
    static func getLanguage(completion: @escaping(Result<[LanguageOld], LanguageError>) -> Void) {
        
        // On construit l'url si nécessaire
        let jsonUrlString: String = "https://my-json-server.typicode.com/Aladinbej/marks/matieres"
        //let jsonUrlString: String = "http://idboard.net:9000/student-api/Marks/by-idboard/2020005"
        guard let url: URL = URL(string: jsonUrlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            // On s'assure de récupérer des données de l'api (sous forme de Byte actuellement)
            guard let data: Data = data else {
                completion(.failure(.noDataAvailable))
                return
            }
            
            do {
                
                if (response as? HTTPURLResponse) != nil {
                    //print(response.statusCode)
                    
                    // On transforme le json reçu en objet => ici un tableau d'Article
                    let language: [LanguageOld] = try JSONDecoder().decode([LanguageOld].self, from: data)
                    completion(.success(language))
                    
                } else {
                    // Ici, on pourrait renvoyer une nouvelle completion failure => .noHttpResponse
                }
            } catch let jsonErr {
                print("connot proccess data => \(jsonErr)")
                completion(.failure(.cannotProcessData))
            }
            
            
        }.resume()
    }
    
    static func getDomaines(completion: @escaping(Result<[DomainDto], LanguageError>) -> Void) {
        
        // On construit l'url si nécessaire
        let userConnected: BusinessEntities? = Utility.getUserDefaultsJson(key: UserDefaultsKey.BUSINESS_ENTITIES_CONNECTED, decodingType: BusinessEntities.self)
        
        if let userConnected = userConnected, let idboard = userConnected.idboard, let currentClass = userConnected.idCurrentClass
        {
            let jsonUrlString: String = "http://idboard.net:9000/student-api/Marks/by-idboard-idclass/\(idboard)/\(currentClass)"
            guard let url: URL = URL(string: jsonUrlString) else { return }

            URLSession.shared.dataTask(with: url) { (data, response, error) in
                
                // On s'assure de récupérer des données de l'api (sous forme de Byte actuellement)
                guard let data: Data = data else {
                    completion(.failure(.noDataAvailable))
                    return
                }
                
                do {
                    
                    if (response as? HTTPURLResponse) != nil {
                        //print(response.statusCode)
                        
                        // On transforme le json reçu en objet => ici un tableau de domaines
                        let domaines: [DomainDto] = try JSONDecoder().decode([DomainDto].self, from: data)
                        completion(.success(domaines))
                        
                    } else {
                        // Ici, on pourrait renvoyer une nouvelle completion failure => .noHttpResponse
                    }
                } catch let jsonErr {
                    print("connot proccess data => \(jsonErr)")
                    completion(.failure(.cannotProcessData))
                }
                
                
            }.resume()
        }
        else
        {
            completion(.failure(.noDataAvailable))
        }
        
    }
}


