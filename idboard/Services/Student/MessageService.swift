//  MessageService.swift
//  idboard
//
//  Created by CampusID001 on 03/04/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

enum MessageError: Error{
      case noDataAvailable
      case cannotProcessData
      //case noHttpResponse
    
}

class MessageService{
    
    static func getMessage(completion: @escaping(Result<[Message], MessageError>) -> Void) {

        // On récupère tous les messages de l'idboard connecte
        let userConnected: BusinessEntities? = Utility.getUserDefaultsJson(key: UserDefaultsKey.BUSINESS_ENTITIES_CONNECTED, decodingType: BusinessEntities.self)
        
        if let userConnected = userConnected, let idboard = userConnected.idboard
        {
            // Url de l'api
            let jsonUrlString: String = "http://idboard.net:9000/student-api/Messages/\(idboard)"
            guard let url: URL = URL(string: jsonUrlString) else { return }
            
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                
                // On s'assure de récupérer des données de l'api (sous forme de Byte actuellement)
                guard let data: Data = data else {
                    completion(.failure(.noDataAvailable))
                    return
                }
                
                do {
                    
                    if let response = response as? HTTPURLResponse {
                        print(response.statusCode)
                        
                        // On transforme le json reçu en objet => ici un tableau d'Article
                        let messages: [Message] = try JSONDecoder().decode([Message].self, from: data)
                        completion(.success(messages))
                        
                    } else {
                        // Ici, on pourrait renvoyer une nouvelle completion failure => .noHttpResponse
                    }
                } catch let jsonErr {
                    print("connot proccess data => \(jsonErr)")
                    completion(.failure(.cannotProcessData))
                }
                
                
            }.resume()
        }
        else
        {
            completion(.failure(.noDataAvailable))
        }
            
    }
    
    static func getBirthday(completion: @escaping(Result<[BirthdayDateDto], MessageError>) -> Void) {
        
        // Url de l'api
        let jsonUrlString: String = "http://idboard.net:9000/student-api/personalInformations/birthday"
        guard let url: URL = URL(string: jsonUrlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            // On s'assure de récupérer des données de l'api (sous forme de Byte actuellement)
            guard let data: Data = data else {
                completion(.failure(.noDataAvailable))
                return
            }
            
            do {
                
                if let response = response as? HTTPURLResponse {
                    print(response.statusCode)
                    
                    // On transforme le json reçu en objet => ici un tableau d'Article
                    let messages: [BirthdayDateDto] = try JSONDecoder().decode([BirthdayDateDto].self, from: data)
                    completion(.success(messages))
                    
                } else {
                    // Ici, on pourrait renvoyer une nouvelle completion failure => .noHttpResponse
                }
            } catch let jsonErr {
                print("connot proccess data => \(jsonErr)")
                completion(.failure(.cannotProcessData))
            }
            
            
        }.resume()
        
        
    }
    
}
