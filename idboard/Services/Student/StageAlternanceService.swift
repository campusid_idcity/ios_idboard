//
//  StageAlternanceService.swift
//  idboard
//
//  Created by Marius Marciniak on 11/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

enum StageAlternanceError: Error {
    case noDataAvailable
    case cannotProcessData
    //case noHttpResponse
}

class StageAlternanceService {
    
    static func getStageAlternance(completion: @escaping(Result<[StageAlternance], StageAlternanceError>) -> Void) {
        
        // On construit l'url si nécessaire
        let jsonUrlString: String = "http://idboard.net:9000/student-api/Stages/currents"
        guard let url: URL = URL(string: jsonUrlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            // On s'assure de récupérer des données de l'api (sous forme de Byte actuellement)
            guard let data: Data = data else {
                completion(.failure(.noDataAvailable))
                return
            }
            
            do {
                
                if (response as? HTTPURLResponse) != nil {
                    //print(response.statusCode)
                    
                    // On transforme le json reçu en objet => ici un tableau d'Article
                    let stageAlternance: [StageAlternance] = try JSONDecoder().decode([StageAlternance].self, from: data)
                    completion(.success(stageAlternance))
                    
                } else {
                    // Ici, on pourrait renvoyer une nouvelle completion failure => .noHttpResponse
                }
            } catch let jsonErr {
                print("connot proccess data => \(jsonErr)")
                completion(.failure(.cannotProcessData))
            }
            
            
        }.resume()
    }
}
