//
//  PlanningService.swift
//  idboard
//
//  Created by Marius Marciniak on 27/03/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

enum PlanningError: Error {
    case noDataAvailable
    case cannotProcessData
    //case noHttpResponse
}

class PlanningService {
    
    static func getPlannings(completion: @escaping(Result<[Courses], PlanningError>) -> Void) {
        
        // On construit l'url si nécessaire
        let jsonUrlString: String = "http://idboard.net:9000/student-api/Courses/courses-in-dates/66/2020-01-01/2020-07-01"
        
        guard let url: URL = URL(string: jsonUrlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            // On s'assure de récupérer des données de l'api (sous forme de Byte actuellement)
            guard let data: Data = data else {
                completion(.failure(.noDataAvailable))
                return
            }
            
            do {
                
                if (response as? HTTPURLResponse) != nil {
                    //print(response.statusCode)
                    
                    // On transforme le json reçu en objet => ici un tableau d'Article
                    let plannings: [Courses] = try JSONDecoder().decode([Courses].self, from: data)
                    completion(.success(plannings))
                    
                } else {
                    // Ici, on pourrait renvoyer une nouvelle completion failure => .noHttpResponse
                }
            } catch let jsonErr {
                print("connot proccess data => \(jsonErr)")
                completion(.failure(.cannotProcessData))
            }
            
            
        }.resume()
    }
}
