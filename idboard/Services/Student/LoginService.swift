//
//  LoginService.swift
//  idboard
//
//  Created by CampusID002 on 04/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

enum LoginError: Error {
    case noDataAvailable
    case cannotProcessData
    //case noHttpResponse
}


class LoginService {
    
    static func resetPsw(idboard: String,completion: @escaping(Result<Reset?, LoginError>) -> Void) {

        let jsonUrlString: String = "https://my-json-server.typicode.com/get/client"
        guard let url: URL = URL(string: jsonUrlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            // On s'assure de récupérer des données de l'api (sous forme de Byte actuellement)
            guard let data: Data = data else {
                completion(.failure(.noDataAvailable))
                return
            }
            do {
                if (response as? HTTPURLResponse) != nil {
                    //print(response.statusCode)
                    let reset: Reset? = try JSONDecoder().decode(Reset.self, from: data)
                    completion(.success(reset))
                } else {
                    // Ici, on pourrait renvoyer une nouvelle completion failure => .noHttpResponse
                }
            } catch let jsonErr {
                print("connot proccess data => \(jsonErr)")
                completion(.failure(.cannotProcessData))
            }
        }.resume()
    }
    
    static func loginPost(user: String, psw: String, completion: @escaping(Result<Login,LoginError>) -> Void) {
        
        let Url = String(format: "https://my-json-server.typicode.com/posts")
        guard let serviceUrl = URL(string: Url) else { return }
        var parameters: [String: Any] = [:]
        parameters["title"] = user
        parameters["body"] = psw
        parameters["userId"] = 24
           
        var request = URLRequest(url: serviceUrl)
        request.httpMethod = "POST"
        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) else {
            return
        }
        request.httpBody = httpBody
        //request.timeoutInterval = 20
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in

            // On s'assure de récupérer des données de l'api (sous forme de Byte actuellement)
            guard let data: Data = data else {
                completion(.failure(.noDataAvailable))
                return
            }
            do {
                if let response = response as? HTTPURLResponse {
                    print("status code => \(response.statusCode)")
                    // On transforme le json reçu en objet => ici un tableau d'Article
                    let login: Login = try JSONDecoder().decode(Login.self, from: data)
                    completion(.success(login))
                    
                } else {
                    // Ici, on pourrait renvoyer une nouvelle completion failure => .noHttpResponse
                }
            } catch let jsonErr {
                print("connot proccess data => \(jsonErr)")
                completion(.failure(.cannotProcessData))
            }
        }.resume()
    }
    
    static func login(user: String, psw: String, completion: @escaping(Result<BusinessEntities,LoginError>) -> Void) {
        
        let Url = String(format: "http://idboard.net:9000/student-api/BusinessEntities/\(user)")
        guard let serviceUrl = URL(string: Url) else { return }
           
        var request = URLRequest(url: serviceUrl)
        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
        
            // On s'assure de récupérer des données de l'api (sous forme de Byte actuellement)
            guard let data: Data = data else {
                completion(.failure(.noDataAvailable))
                return
            }
            do {
                if let response = response as? HTTPURLResponse {
                    print("status code => \(response.statusCode)")
                    
                    // On transforme le json reçu en objet => ici un businessEntity
                    let businessEntities: BusinessEntities = try JSONDecoder().decode(BusinessEntities.self, from: data)

                    // On vérifie qu'aucune erreur n'est renvoyé par l'API
                    if businessEntities.idboard != nil {
                        
                        // Ici on stock dans la mémoire du téléphone l'utilisateur connecté + un boolean afin que l'utilisateur n'est pas à repasser par l'écran de connexion lors d'une prochaine utilisation de l'app.
                        Utility.setUserDefaultsJson(object: businessEntities, key: UserDefaultsKey.BUSINESS_ENTITIES_CONNECTED)
                        UserDefaults.standard.set(true, forKey: UserDefaultsKey.USER_IS_LOGGED)
                        
                        // Completion handler => remplace le mot clé "return" dans une fonction asynchrone
                        completion(.success(businessEntities))
                    } else {
                         completion(.failure(.noDataAvailable))
                    }
                    
                } else {
                    // Ici, on pourrait renvoyer une nouvelle completion failure => .noHttpResponse
                }
            } catch let jsonErr {
                print("connot proccess data => \(jsonErr)")
                completion(.failure(.cannotProcessData))
            }
        }.resume()
    
    }
}


