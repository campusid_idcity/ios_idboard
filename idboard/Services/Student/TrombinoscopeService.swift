//
//  TrombinoscopeService.swift
//  idboard
//
//  Created by CampusID002 on 11/06/2020.
//  Copyright © 2020 CampusID. All rights reserved.
//

import Foundation

class TrombinoscopeService {
    
    static func getTrombinoscope(completion: @escaping(Result<TrombinoscopeDto, MessageError>) -> Void) {
        
        // On récupère le numéro d'idboard stocké en userDefault
        let businessEntities: BusinessEntities? = Utility.getUserDefaultsJson(key: UserDefaultsKey.BUSINESS_ENTITIES_CONNECTED, decodingType: BusinessEntities.self)
        
        if let businessEntities = businessEntities, let idboard = businessEntities.idboard {
            
            // On construit l'url si nécessaire
            let jsonUrlString: String = "http://idboard.net:9000/student-api/Trombinoscope/by-idboard/\(idboard)"
            guard let url: URL = URL(string: jsonUrlString) else { return }
            print(jsonUrlString)
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                
                // On s'assure de récupérer des données de l'api (sous forme de Byte actuellement)
                guard let data: Data = data else {
                    completion(.failure(.noDataAvailable))
                    return
                }
                
                do {
                    // On s'assure qu'il existe une réponse du serveur
                    if let response = response as? HTTPURLResponse {
                        print(response.statusCode)
                        
                        // On transforme le json reçu en objet => ici un TrombinoscopeDto. En cas d'erreur au niveau du parseur, renvoit une exception géré dans le "catch let" plus bas
                        let trombinoscope: TrombinoscopeDto = try JSONDecoder().decode(TrombinoscopeDto.self, from: data)
                        
                        // On renvoit le trombinoscope
                        completion(.success(trombinoscope))
                        
                    } else {
                        // Ici, on pourrait renvoyer une nouvelle completion failure => .noHttpResponse
                    }
                } catch let jsonErr {
                    print("connot proccess data => \(jsonErr)")
                    completion(.failure(.cannotProcessData))
                }
            }.resume()
        } else {
            completion(.failure(.noDataAvailable))
        }
        
    }
}
